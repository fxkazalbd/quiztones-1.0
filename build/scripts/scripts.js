(function ($) {
    $(document).ready(function() {

        /* -------------------------------------
         * carousel
        /* ---------------------------------- */
        $('.carousel').slick({
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: false,
            dots: true,
        });

    });
})(jQuery);
